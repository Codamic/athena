(defproject codamic/athena "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "https://gitlab.com/Codamic/athena"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/spec.alpha "0.2.176"]
                 [codamic/hellhound.core  "1.0.0-SNAPSHOT"]
                 [codamic/hellhound.utils "1.0.0-SNAPSHOT"]
                 [expound "0.7.2"]
                 [orchestra "2018.12.06-2"]
                 [clj-http "3.9.1"]
                 [cheshire "5.8.1"]
                 [com.cemerick/url "0.1.1"]
                 [com.taoensso/carmine "2.19.1"]]



  :main  athena.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
