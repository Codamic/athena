(ns athena.core
 (:require
   [hellhound.system :as sys]
   [hellhound.system.development :as d]
   [athena.system :as system])
 (:gen-class))

(d/setup-development athena.core  'system/dev-system)

(defn start-system
  []
  (sys/set-system! system/dev-system)
  (sys/start!))

(defn stop-system
  []
  (sys/stop!))

(defn -main
  [& args]
  (let [[command & args] args]
    (cond
      (= command "start") (start-system)
      (= command "stop")  (stop-system)
      :else (start-system))))
