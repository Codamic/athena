(ns athena.system
  (:require
   [hellhound.message :as msg]
   [hellhound.components.pretty-printer :as pp]
   [hellhound.components.webserver :as webserver]
   [athena.components.router :as router]
   [athena.routes :as routes]
   [athena.http.handlers :as handlers]
   [hellhound.message :as message]))

(msg/make-type-helpers :not-found :command)

(defn dev-system
  []
  {:components [(webserver/factory)
                (router/factory routes/routes)
                pp/printer]

   :workflow
   [
    {:hellhound.workflow/source ::webserver/webserver
     :hellhound.workflow/sink ::router/router}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/sink ::pp/printer}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/filter not-found-type?
     :hellhound.workflow/map #(handlers/not-found-response %)
     :hellhound.workflow/sink ::webserver/webserver}

    {:hellhound.workflow/source ::router/router
     :hellhound.workflow/filter (every-pred command-type? routes/echo-command?)
     :hellhound.workflow/map #(handlers/echo %)
     :hellhound.workflow/sink ::webserver/webserver}]



   :execution {:mode :multi-thread}
   :logger {:level :info}
   :config
   {::webserver/webserver {:host "localhost" :port 3000}}})
