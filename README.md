# athena

The godess of wisdom, the geek :))

## Requirements
* JDK >= 1.8
* [Leiningen](https://leiningen.org/)

## Usage
For development spin up your mighty FG42/Emacs (with clojure plugin enabled) and
open a clojure file in the project then `M-x cider-jack-in` and after a while you
have the REPL up and running.

For running the system outside of REPL:

```
$ lein run
```


## License

Copyright © 2019 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
